<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
	/*
	                        __           __             __             
	                       /\ \         /\ \__         /\ \__          
	  ___     __     _ __  \_\ \    ____\ \ ,_\    __  \ \ ,_\    __   
	 /'___\ /'__`\  /\`'__\/'_` \  /',__\\ \ \/  /'__`\ \ \ \/  /'__`\ 
	/\ \__//\ \L\.\_\ \ \//\ \L\ \/\__, `\\ \ \_/\ \L\.\_\ \ \_/\  __/ 
	\ \____\ \__/.\_\\ \_\\ \___,_\/\____/ \ \__\ \__/.\_\\ \__\ \____\
	 \/____/\/__/\/_/ \/_/ \/__,_ /\/___/   \/__/\/__/\/_/ \/__/\/____/
	*/
	if(!isset($_GET['id'])) {
        $current = "1";
    } else {
        $current = $_GET['id'];
    }
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
    if($_SESSION['uid']==1) {
        $query = "SELECT * FROM cardstatement";
    } else {
        $query = "SELECT * FROM cardstatement WHERE uid = '" . $_SESSION['uid'] . "'";
    }

    $countrow = mysqli_query($con, $query);
    $num_rows = mysqli_num_rows($countrow);

    //////////////

    if($_SESSION['uid']==1) {
        $query = "SELECT * FROM cardstatement WHERE transno = '" . $current . "'";
    } else {
        $offset = $current-1;
        $query = "SELECT * FROM cardstatement WHERE uid = '" . $_SESSION['uid'] . "' LIMIT 1 OFFSET " . $offset;
    }
    $result = mysqli_query($con, $query) or die("Data not found");
    $row=mysqli_fetch_array($result);

    $tnoo = $row['transno'];
    if($current>1) {
        $newid = $current-1;
        $prevbtn = "cardstate.php?id=".$newid;
    } else {
        $prevbtn = "cardstate.php?id=".$current;
    }
    if($current<$num_rows) {
        $newid =$current+1;
        $nextbtn = "cardstate.php?id=".$newid;
    } else {
        $nextbtn = "cardstate.php?id=".$current;
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Card Statement</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
    function editbox() {
        var txtseller = document.getElementById("sellerno");
        var txtproduct = document.getElementById("product");
        var txtprice = document.getElementById("price");
        var btnsave = document.getElementById('save');
        txtseller.readOnly =false;
        txtproduct.readOnly =false;
        txtprice.readOnly =false;
        btnsave.disabled = false;
    }
    </script>
</head>

<body>

    <div id="wrapper">
		<?php include "com.header.php"; ?>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header" style="color:#0066CC">Credit Card Statement</h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
	                <div class="col-lg-6">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            Credit Card Transaction
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-12">
	                                    <form role="form" name="card" action="savecard.php" method="post">
	                                     
	                                        <div class="form-group">
	                                            <label>Transaction No.</label>
	                                            <input  name="transno" class="form-control" placeholder="Enter text" readonly value="<?php echo $row['transno'];?>">
	                                        </div>

	                                        <label>User ID No.</label>
	                                        <div class="form-group input-group">
	                                            <span class="input-group-addon">@</span>
	                                            <input name="uid" type="text" class="form-control" placeholder="User ####" readonly value="<?php echo $row['uid'];?>">
	                                        </div>

	                                        <div class="form-group">
	                                            <label>Credit Card No.</label>
	                                            <input class="form-control" placeholder="Enter Creditcard No." readonly  value="<?php echo $row['number'];?>">
	                                        </div>

	                                        <div class="form-group">
	                                            <label>Date</label>
	                                            <input type="date" class="form-control" placeholder="Enter Date" readonly value="<?php echo $row['date'];?>">
	                                        </div>
	                                    
	                                        <div class="form-group">
	                                            <label>Seller No.</label>
	                                            <input id="sellerno" name="sellerno" class="form-control" placeholder="Enter Seller No." readonly  value="<?php echo $row['sellerno'];?>">
	                                        </div>

	                                        <div class="form-group">
	                                            <label>Product</label>
	                                            <input id="product" name="product" class="form-control" placeholder="Enter Product" readonly  value="<?php echo $row['product'];?>">
	                                        </div>

	                                        <label>Price</label>
	                                        <div class="form-group input-group">
	                                            <span class="input-group-addon">$</span>
	                                            <input id="price" name="price" type="text" class="form-control" readonly  value="<?php echo $row['price'];?>">
	                                            <span class="input-group-addon">.00</span>
	                                        </div>
	                                        <table width='100%'><tr>
	                                        <td width ='25%'><button type="button" class="btn btn-default btn-circle btn-lg"  onClick="location.href='<?php echo $prevbtn; ?>'"><i class="fa fa-angle-left" ></i></button></td>
	                                        
	                                        <td width ='50%'>   
	                                                <button type="button" class="btn btn-primary btn-circle btn-lg" onClick="editbox()"><i class="fa fa-edit"></i></button>
	                                                <button id="save"  name = "save" type="submit" class="btn btn-success btn-circle btn-lg" disabled><i class="fa fa-save"></i></button>
	                                                <button type="button" class="btn btn-warning btn-circle btn-lg" onClick="location.href='insertcard.php'" ><i class="fa fa-plus"></i></button>
	                                                <button type="button" class="btn btn-danger btn-circle btn-lg" onClick="location.href='deletecard.php?tno=<?php echo $tnoo; ?>'" ><i class="fa fa-times"></i></button>
	                                        </td>
	                                        <td  width ='25%'><button type="button" class="btn btn-default btn-circle btn-lg" onClick="location.href='<?php echo $nextbtn; ?>'"><i class="fa fa-angle-right"></i></button></td>
	                                        </tr></table>

	                                    </form>
	                                    <?php  
	                                            $result->close();
	                                            $con->close();?>
	                                    </div>
	                                </div>
	                                <!-- /.col-lg-6 (nested) -->
	                            
	                            <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->
	                </div>
	                <!-- /.col-lg-12 -->
	            </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include 'footer.php' ?>
    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>