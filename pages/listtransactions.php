<!DOCTYPE html>
<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
/*
 ___            __        __                                    
/\_ \          /\ \__    /\ \__                                 
\//\ \     ____\ \ ,_\   \ \ ,_\  _ __    __      ___     ____  
  \ \ \   /',__\\ \ \/    \ \ \/ /\`'__\/'__`\  /' _ `\  /',__\ 
   \_\ \_/\__, `\\ \ \_    \ \ \_\ \ \//\ \L\.\_/\ \/\ \/\__, `\
   /\____\/\____/ \ \__\    \ \__\\ \_\\ \__/.\_\ \_\ \_\/\____/
   \/____/\/___/   \/__/     \/__/ \/_/ \/__/\/_/\/_/\/_/\/___/ 

*/
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List Transaction</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="xml2json.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

		<?php include "com.header.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-heading">
                            <h2>Credit Card Information</h2>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive" id="data" ng-controller="myCtrl">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Transaction No.</th>
                                            <th>User ID</th>
                                            <th>Date</th>
                                            <th>Seller no</th>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Credit card No.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="x in details">
											<td>{{x.transno}}</td>
											<td>{{x.uid}}</td>
											<td>{{x.date}}</td>
											<td>{{x.sellerno}}</td>
											<td>{{x.product}}</td>
											<td>{{x.price}}</td>
											<td>{{x.number}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <script>
                                var app = angular.module('myApp', []);
                                app.controller('myCtrl', function ($scope, $http) {
                                    $http.get("transactions.php",
                                            {
                                                transformResponse: function (cnv) {
                                                    var x2js = new X2JS();
                                                    var aftCnv = x2js.xml_str2json(cnv);
                                                    return aftCnv;
                                                }
                                            })
                                    .success(function (response) {
                                        $scope.details = response.response.transactions.transaction;
                                        console.log(response.response.transactions.transaction);
                                    });
                                });
                            </script>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include 'footer.php' ?>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
