<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>

<?php
	/*
	  __          ___           __                                    __     
	 /\ \        /\_ \         /\ \__                                /\ \    
	 \_\ \     __\//\ \      __\ \ ,_\    __    ___     __     _ __  \_\ \   
	 /'_` \  /'__`\\ \ \   /'__`\ \ \/  /'__`\ /'___\ /'__`\  /\`'__\/'_` \  
	/\ \L\ \/\  __/ \_\ \_/\  __/\ \ \_/\  __//\ \__//\ \L\.\_\ \ \//\ \L\ \ 
	\ \___,_\ \____\/\____\ \____\\ \__\ \____\ \____\ \__/.\_\\ \_\\ \___,_\
	 \/__,_ /\/____/\/____/\/____/ \/__/\/____/\/____/\/__/\/_/ \/_/ \/__,_ /
	*/
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	if($_SESSION['uid']!=1) {
		$query = "SELECT * FROM cardstatement WHERE transno=".$_GET['tno'];
		$result = mysqli_query($con, $query) or die("Data not found.");
		$result = mysqli_fetch_array($result);
		if($result['uid']!=$_SESSION['uid']) {
			die("Naughty Boy");
		}
	}
	$query = "DELETE FROM cardstatement WHERE transno=".$_GET['tno'];
	$result = mysqli_query($con, $query) or die("Failed to delete.");
	redirect('cardstate.php');  
?>