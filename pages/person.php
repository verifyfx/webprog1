	<?php
	/*
	      __                     __           ___                                  
	     /\ \                   /\ \         /\_ \                   __            
	  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
	 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
	/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
	\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
	 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
	                                                             /\____/           
	                                                             \_/__/            
	*/
	// Start session
	session_start();
	// Include required functions file
	require_once('includes/functions.inc.php');
	// Check login status... if not logged in, redirect to login screen
	if (check_login_status() == false) {
	    redirect('login.php');
	    }
	?>

	<?php
		/*
		                       ___      ___             
		                     /'___\ __ /\_ \            
		 _____   _ __   ___ /\ \__//\_\\//\ \      __   
		/\ '__`\/\`'__\/ __`\ \ ,__\/\ \ \ \ \   /'__`\ 
		\ \ \L\ \ \ \//\ \L\ \ \ \_/\ \ \ \_\ \_/\  __/ 
		 \ \ ,__/\ \_\\ \____/\ \_\  \ \_\/\____\ \____\
		  \ \ \/  \/_/ \/___/  \/_/   \/_/\/____/\/____/
		   \ \_\                                        
		    \/_/                                        
		*/
		include "includes/config.inc.php";
		$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		if($con->connect_error){
    		die("Connection failed: ".$con->connect_error);
		}
		$query = "SELECT * FROM personinfo WHERE id=".$_SESSION['uid'];
		if($_SESSION['uid'] == 1) $query = "SELECT * FROM personinfo";
		$result = mysqli_query($con, $query) or die("Data not found.");
		$count = $result->num_rows;
	?>
	<!DOCTYPE html>
	<html lang="en">

	<head>

	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">

	    <title>Profile</title>

	    <!-- Bootstrap Core CSS -->
	    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- MetisMenu CSS -->
	    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

	</head>

	<body>

	    <div id="wrapper">

	        <?php include 'com.header.php'; ?>

	        <!-- Page Content -->
	        <div id="page-wrapper">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-lg-12">
	                        <div class="chat-panel panel panel-default">
	                        <div class="panel-heading">
	                            <i class="fa fa-comments fa-fw"></i>
	                            Information about <?php echo $count; ?> people.
	                        </div>
	                        <!-- /.panel-heading -->
	                        <div class="panel-body">
	                            <ul class="chat">
	                            		<?php
											while ($row = mysqli_fetch_array($result)) {
										?>
	                                <li class="left clearfix">
	                                    <span class="chat-img pull-left">
	                                        <img src="user.png" alt="User Avatar" class="img-circle" />
	                                    </span>

	                                    <div class="chat-body clearfix">
	                                        <div class="header">
	                                            <strong class="primary-font"><?php echo $row['firstname'];?></strong>
	                                        </div>
                                        <table border="0"><tbody>
                                            <tr>
                                            <td><b>Name:</b></td>
                                            <td><?php echo $row['firstname'] . " " .$row['lastname'];?></td>
                                            </tr>
                                            <tr>
                                            <td><b>Location:</b></td>
                                            <td><?php echo $row['city'];?></td>
                                            </tr>
                                            <tr>
                                            <td><b>Telephone:</b></td>
                                            <td><?php echo $row['telephone'];?></td>
                                            </tr>
                                            <tr>
                                            <td><b>E-mail</b></td>
                                            <td><?php echo $row['email'];?></td>
                                            </tr>
                                            </tbody>
                                        </table>
	                                    </div>
	                                </li>
	                                	                                    <?php
	                                    	}
	                                    ?>
	                            </ul>
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel .chat-panel -->
	                    </div>
	                    <!-- /.col-lg-12 -->
	                </div>
	                <!-- /.row -->
	            </div>
	            <!-- /.container-fluid -->
	        </div>
	        <!-- /#page-wrapper -->

	    </div>
	    <!-- /#wrapper -->
		<?php include 'footer.php' ?>
	    <!-- jQuery -->
	    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

	    <!-- Bootstrap Core JavaScript -->
	    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	    <!-- Metis Menu Plugin JavaScript -->
	    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

	    <!-- Custom Theme JavaScript -->
	    <script src="../dist/js/sb-admin-2.js"></script>

	</body>

	</html>
