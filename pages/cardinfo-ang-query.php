<?php
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>

<?php 
require_once('includes/config.inc.php');
$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
if($con->connect_error){
    die("Connection failed: ".$con->connect_error);
}
?>
<?php 
    $query = "SELECT `personinfo`.`firstname` , `cardinfo`.`number` , `cardinfo`.`issuer` , `cardinfo`.`exp` , `cardinfo`.`limit` , `cardinfo`.`currency` , `cardinfo`.`uid`\n"
    . "FROM cardinfo\n"
    . "INNER JOIN personinfo ON personinfo.id = cardinfo.uid";
    $result = mysqli_query($con, $query) or die("Can't select card");
    $datacard = array();
    while($row = mysqli_fetch_assoc($result)) {
        $datacard[] = $row;
    }
    echo json_encode($datacard);
?>