<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>

<?php
	/*
	                        __                 ___         
	                       /\ \  __          /'___\        
	  ___     __     _ __  \_\ \/\_\    ___ /\ \__/  ___   
	 /'___\ /'__`\  /\`'__\/'_` \/\ \ /' _ `\ \ ,__\/ __`\ 
	/\ \__//\ \L\.\_\ \ \//\ \L\ \ \ \/\ \/\ \ \ \_/\ \L\ \
	\ \____\ \__/.\_\\ \_\\ \___,_\ \_\ \_\ \_\ \_\\ \____/
	 \/____/\/__/\/_/ \/_/ \/__,_ /\/_/\/_/\/_/\/_/ \/___/ 
	*/
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	$query = "SELECT * FROM cardinfo WHERE uid=".$_SESSION['uid'];
	if($_SESSION['uid'] == 1) $query = "SELECT * FROM cardinfo";
	$result = mysqli_query($con, $query) or die("Data not found.");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Card information</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

		<?php include 'com.header.php'; ?>

        <div id="page-wrapper">
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Credit card information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Number</th>
                                            <th>Issuer</th>
                                            <th>Expire</th>
                                            <th>Limit</th>
                                            <th>Currency</th>
                                        </tr>
                                    </thead>
                                    <tbody>
		                                <?php 
		                                    $curr = 0;
		                                    while ($row = mysqli_fetch_array($result)) {
		                                        $str = 'odd';
		                                        if($curr==0) {
		                                            $str = 'even';
		                                            $curr = 1;
		                                        } else { 
		                                            $curr = 0;
		                                        }
		                                        echo '<tr role="row" class="gradeA '. $str .'">';
		                                        echo '<td>' . $row['name'] . '</td>';
		                                        echo '<td>' . $row['number'] . '</td>';
		                                        echo '<td>' . $row['issuer'] . '</td>';
		                                        echo '<td>' . $row['exp'] . '</td>';
		                                        echo '<td>' . $row['limit'] . '</td>';
		                                        echo '<td>' . $row['currency'] . '</td>';
		                                        echo "</tr>";
		                                    }
		                                 ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php include 'footer.php' ?>
    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
