<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
	/*
	 __                                                   __                                  
	/\ \__                                               /\ \__  __                           
	\ \ ,_\  _ __    __      ___     ____     __      ___\ \ ,_\/\_\    ___     ___     ____  
	 \ \ \/ /\`'__\/'__`\  /' _ `\  /',__\  /'__`\   /'___\ \ \/\/\ \  / __`\ /' _ `\  /',__\ 
	  \ \ \_\ \ \//\ \L\.\_/\ \/\ \/\__, `\/\ \L\.\_/\ \__/\ \ \_\ \ \/\ \L\ \/\ \/\ \/\__, `\
	   \ \__\\ \_\\ \__/.\_\ \_\ \_\/\____/\ \__/.\_\ \____\\ \__\\ \_\ \____/\ \_\ \_\/\____/
	    \/__/ \/_/ \/__/\/_/\/_/\/_/\/___/  \/__/\/_/\/____/ \/__/ \/_/\/___/  \/_/\/_/\/___/ 
	*/
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	if($_SESSION['uid']==1) {
        $query = "SELECT * FROM cardstatement";
    } else {
        $query = "SELECT * FROM cardstatement WHERE uid = '" . $_SESSION['uid'] . "'";
    }
    $result = mysqli_query($con, $query) or die("Data not found.");

	header("Content-Type: xml");

	$xmlDoc = new DOMDocument();
	$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
	$transactions = $root->appendChild($xmlDoc->createElement("transactions"));
	while ($row = mysqli_fetch_array($result)) {
		$cur = $transactions->appendChild($xmlDoc->createElement("transaction"));
		$data = $cur->appendChild($xmlDoc->createElement("transno", $row['transno']));
		$data = $cur->appendChild($xmlDoc->createElement("uid", $row['uid']));
		$data = $cur->appendChild($xmlDoc->createElement("date", $row['date']));
		$data = $cur->appendChild($xmlDoc->createElement("sellerno", $row['sellerno']));
		$data = $cur->appendChild($xmlDoc->createElement("product", $row['product']));
		$data = $cur->appendChild($xmlDoc->createElement("price", $row['price']));
		$data = $cur->appendChild($xmlDoc->createElement("number", $row['number']));
	}

	echo $xmlDoc->saveHTML(); 
?>