<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php 
	/*
	                                   __                             __     
	 __                               /\ \__                         /\ \    
	/\_\    ___     ____     __   _ __\ \ ,_\   ___     __     _ __  \_\ \   
	\/\ \ /' _ `\  /',__\  /'__`\/\`'__\ \ \/  /'___\ /'__`\  /\`'__\/'_` \  
	 \ \ \/\ \/\ \/\__, `\/\  __/\ \ \/ \ \ \_/\ \__//\ \L\.\_\ \ \//\ \L\ \ 
	  \ \_\ \_\ \_\/\____/\ \____\\ \_\  \ \__\ \____\ \__/.\_\\ \_\\ \___,_\
	   \/_/\/_/\/_/\/___/  \/____/ \/_/   \/__/\/____/\/__/\/_/ \/_/ \/__,_ /
	*/
	require_once('includes/config.inc.php');
	$err = "";

	if(isset($_POST['number']) &&  !empty($_POST['number'])) {
	    $uid = $_SESSION['uid'];
	    $date = $_POST['date'];
	    $number = $_POST['number'];
	    $seller = $_POST['sellerno'];
	    $product = $_POST['product'];
	    $price = $_POST['price'];
	  
		$link =mysqli_connect(DB_HOSTNAME, DB_USERNAME,DB_PASSWORD,DB_DATABASE) or die("Could not connect to host.");
		$query = "ALTER TABLE cardstatement AUTO_INCREMENT=1";
		mysqli_query($link,$query);
		$query = "INSERT INTO cardstatement(date,sellerno,product,price,number,uid) VALUES ('".$date."','".$seller."','".$product."','".$price."','".$number."','".$uid."')"; 

		$result = mysqli_query($link, $query) or die("Data not found");
	  
	  	if($uid == 1) {
	    	$countrow = mysqli_query($link,"SELECT * FROM cardstatement");
	  	} else {
	    	$countrow = mysqli_query($link,"SELECT * FROM cardstatement WHERE uid ='".$uid."'");
	  	}

		//$num_rows = mysqli_num_rows($countrow);
		$query = "SELECT * FROM cardstatement ORDER BY transno DESC LIMIT 1";
		$result = mysqli_query($link, $query) or die("Data not found");
		$row=mysqli_fetch_array($result);
		$num = $row[transno];
		//$result->close();
		$countrow->close();
		$link->close();
		header("location:cardstate.php?id=".$num);
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Card Statement</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <?php include "com.header.php"; ?>

        <div id="page-wrapper" style="min-height: 355px;">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="color:#0066CC">Credit Card Statement</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>

			<div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Credit Card Transaction
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" name="card" action="insertcard.php" method="post">
                                        <!--
                                        <div class="form-group">
                                            <label>Transaction No.</label>
                                            <input  name="transno" class="form-control" placeholder="Enter text"  value="" disabled>
                                        </div>

										<label>User ID No.</label>
                                        <div class="form-group input-group">
											<span class="input-group-addon">@</span>
                                            <input name="uid" type="text" class="form-control" placeholder="User ####" value="" disabled>
                                        </div>
                                        -->
                                        <input type="hidden" name="chk" value="1">
										<div class="form-group">
                                            <label>Credit Card No.</label>
                                            <input name="number" class="form-control" placeholder="Enter Creditcard No."  value="" required>
                                        </div>

										<div class="form-group">
                                            <label>Date</label>
                                            <input name="date" type="date" class="form-control" placeholder="Enter Date" value="" required>
                                        </div>
                                    
										<div class="form-group">
                                            <label>Seller No.</label>
                                            <input id="sellerno" name="sellerno" class="form-control" placeholder="Enter Seller No." value="" required>
                                        </div>

										<div class="form-group">
                                            <label>Product</label>
                                            <input id="product" name="product" class="form-control" placeholder="Enter Product" value="" required>
                                        </div>

                                        <label>Price</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">$</span>
                                            <input id="price" name="price" type="text" class="form-control"   value="" required>
                                            <span class="input-group-addon">.00</span>
                                        </div>
										<table width='100%'><tr>
                                        <td width ='25%'></td>
										
										<td width ='50%'>	
												<button id="save"  name = "save" type="submit" class="btn btn-success btn-circle btn-lg"><i class="fa fa-save"></i></button>
												<button type="button" class="btn btn-danger btn-circle btn-lg" onClick="location.href='cardstate.php?id=". $num_rows ."'" ><i class="fa fa-remove"></i></button>
												<BR><?php echo $err; ?>
										<td  width ='25%'></td>
										</tr></table>

                                    </form>
									
									</div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>