<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
if($_SESSION['uid']!=1) die();
?>

<?php
$q=$_GET["q"];

///////////////////
///////////
////////
/////


$xmlDoc=new DOMDocument();

if($q=='Google') {
	$xmlDoc->load("https://news.google.com/news?cf=all&hl=en&pz=1&ned=us&output=rss");
} else if($q=='Sanook') {
	$xmlDoc->load("http://rssfeeds.sanook.com/rss/feeds/sanook/hitech.computer.index.xml");
} else {
	die("No, Bad!");
}


$channel=$xmlDoc->getElementsByTagName('channel')->item(0);
//$channel=$xmlDoc;


//lookup all links from the xml file if length of q>0
$hint="";
for($i=0;$i<$channel->getElementsByTagName('link')->length-1;$i++) {
	//$hint='<p><a href="' . $link . '">' . $title . '</a><br />' . $desc . '</p>';

	$link=$channel->getElementsByTagName('link')->item($i)->childNodes->item(0)->nodeValue;
	$title=$channel->getElementsByTagName('title')->item($i)->childNodes->item(0)->nodeValue;
	$desc=$channel->getElementsByTagName('description')->item($i)->childNodes->item(0)->nodeValue;

	
	$hint=$hint . '<p><a href="' . $link . '">' . $title . '</a><br />' . $desc . '</p>';
}

// or to the correct values
if ($hint=="")
{
  $response="<h4>no news</h4>";
}
else
{
  $response=$hint;
}

//output the response
echo $response;
?> 