<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>

<?php
	if(!isset($_GET['uid'])) die("no UID provided");
	require_once("includes/config.inc.php");
?>
<?php
	require('fpdf.php');
	class PDF extends FPDF {
		function Header() {
			// Logo
			$this->Image('logo.png',5,5,30,30);
			// Arial bold 15
			$this->SetFont('Arial','B',15);
			// Move to the right
			$this->Cell(80);
			// Title
			$this->Cell(30,10,'EGCI 427 Web Programming',0,0,'C');
			// Line break
			$this->Ln();
			// Title
			$this->Cell(80);

			$this->Cell(30,10,'Suphakrit Phantharat\'s Report',0,0,'C');
			// Line break
			$this->Ln(20);
			$this->Cell(190,0,'',1,0,'C');
			$this->Ln(10);


		}

		function tblGen() {
			$header = array('Trans No', 'Date', 'Seller No.', 'Product Name', 'Price', 'Card Number', 'UID');
			$cellWidth = array(22,25,22,30,20,44,10);
			// Colors, line width and bold font of the header
			$this->SetFillColor(166,224,255); //header bg
			$this->SetTextColor(0,0,0); //header text
			$this->SetDrawColor(0); //line color
			$this->SetLineWidth(.1);
			$this->SetFont('','B');
			$this->Cell(8,7,'',0);
			for($i=0;$i<count($header);$i++) {
				$this->Cell($cellWidth[$i],7,$header[$i],1,0,'C',true);
				//width,height,text,border[0=no;1=frame],pointer_location[0=right;1=nextline;2=below],align[L;C;R],CellBackground[T,F],Link[AddLink()];
			}
			$this->Ln();//new line
			// Colors, line width and bold font of the table content
			$this->SetFillColor(224,235,255); //content bg
			$this->SetTextColor(0,0,0); //content text
			$this->SetFont('');
			$data = $this->getSQLData();
			$fillStatus = true;
			foreach($data as $row) {
				$this->Cell(8,7,'',0);
				for($i = 0; $i<count($row);$i++) {
					if($i==4) {
						$this->Cell($cellWidth[$i],6,number_format($row[$i]),'LR',0,'R',$fillStatus);
					} else {
						$this->Cell($cellWidth[$i],6,$row[$i],'LR',0,'L',$fillStatus);
					}
				}
				$this->Ln();
				$fillStatus = !$fillStatus; //alternate row color
			}

			$this->Cell(8,7,'',0);
			$this->Cell(array_sum($cellWidth),0,'','T');
		}

		function getSQLData() {
			$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
			if($con->connect_error){
				die("Connection failed: ".$con->connect_error);
			}
			$query = "SELECT * FROM cardstatement";
			$result = mysqli_query($con, $query) or die("Data not found.");
			$rows = array();
			while(($row = mysqli_fetch_array($result,MYSQLI_NUM))) {
				$last = end($row);
				$newar = array($last);
				array_pop($row);
				$newar = array_merge($newar,$row);
			    $rows[] = $newar;
			}
			return $rows;
		}

		function getUIDName() {
			$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
			if($con->connect_error){
				die("Connection failed: ".$con->connect_error);
			}
			$query = "SELECT * FROM users WHERE id=".$_GET['uid'];
			$result = mysqli_query($con, $query) or die("Data not found.");
			$row = mysqli_fetch_array($result);
			return $row['username'];
		}

		function userDetail() {
			$sUID = 'UID: '.$_GET['uid'];
			$sUSR = 'Username: '.$this->getUIDName();
			$this->SetFillColor(255,165,153);
			$this->Ln(20);
			$this->Cell(20);
			$this->Cell(60,6,$sUID,0,0,'L',true);
			$this->Cell(34);
			$this->Cell(60,6,$sUSR,0,0,'L',true);
		}
	}


	//////////////////////////////////////////////////////////
	$pdf = new PDF();
	$pdf->SetFont('Arial','',12);
	$pdf->AddPage();
	$pdf->tblGen();
	$pdf->userDetail();
	$pdf->Output();
?>