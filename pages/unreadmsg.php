<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>

<?php
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	if($_SESSION['uid']==1) {
        $query = "SELECT * FROM message WHERE flag='0'";
    } else {
        $query = "SELECT * FROM message WHERE uid = '" . $_SESSION['uid'] . "' AND flag='0'";
    }
    $result = mysqli_query($con, $query) or die("Data not found.");

	header("Content-Type: application/json");

	$data;
	while($row = $result->fetch_assoc()) {
		$name = getPMName($row['sender']);
		$row['sender'] = $name;
		$data[] = $row;
	}

	echo json_encode($data);

	function getPMName($target_uid) {
		$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		$query_n = "SELECT firstname, lastname FROM personinfo WHERE id='" . $target_uid . "'";
		$result_n = mysqli_query($con, $query_n) or die("Data not found.");
		$result_n = mysqli_fetch_assoc($result_n);
		return $result_n['firstname'] .' '. $result_n['lastname'];
	}


?>